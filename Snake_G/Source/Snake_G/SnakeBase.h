// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection				
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_G_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	

	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;  

	            
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;                               

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;								

	UPROPERTY(EditDefaultsOnly)
		float DeltaSpeed;

	UPROPERTY(EditDefaultsOnly)
		float MaxSpeed;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;			

	UPROPERTY()
		EMovementDirection LastMoveDirection;

protected:

	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);                            

	void Move();
	
	UFUNCTION(BlueprintCallable)
		void AddSnakeSpeed();

	UFUNCTION(BlueprintCallable)
		void DestroySnakeElements();


	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
};
