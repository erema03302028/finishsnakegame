// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_GGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_G_API ASnake_GGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
